%    mt.Rand = function (arg0) {
%      this.seed = (arg0 < 0 ? -arg0 : arg0) + 131;
%    };
%
%    mt.Rand.__name__ = ['mt', 'Rand'];
%    v1 = mt.Rand.prototype;           
%    v1.__class__ = mt.Rand;
%    v1.addSeed = function (arg0) {
%      this.seed = (this.seed + arg0) % 2147483647 & 1073741823;
%      if (this.seed == 0) {
%        this.seed = arg0 + 1;
%      }
%    };
%
%    v1.clone = function () {
%      var v2 = new mt.Rand(0);
%      v2.seed = this.seed;
%      return v2;
%    };
%
%    v1.atSCJz = function () {
%      return Std.int(this.seed) - 131;
%    };
%   v1.initSeed = function (arg0, arg1) {
%     if (arg1 == null) {
%       arg1 = 5;
%     }
%     var v4 = 0;
%     while (v4 < arg1) {
%       ++v4;
%       var v5 = v4;
%       arg0 ^= arg0 << 7 & 727393536;
%       arg0 ^= arg0 << 15 & 462094336;
%       arg0 ^= arg0 >>> 16;
%       arg0 &= 1073741823;
%       var v6 = 5381;
%       v6 = (v6 << 5) + v6 + (arg0 & 255);
%       v6 = (v6 << 5) + v6 + (arg0 >> 8 & 255);
%       v6 = (v6 << 5) + v6 + (arg0 >> 16 & 255);
%       v6 = (v6 << 5) + v6 + (arg0 >> 24);
%       arg0 = v6 & 1073741823;
%     }
%     this.seed = (arg0 & 536870911) + 131;
%   };
%
%   v1.int = function () {
%     this.seed = this.seed * 16807 % 2147483647;
%     return this.seed & 1073741823;
%   };




Tools.buildMap2(id,plMax);
try {
  var Rand = new mt.Rand(id);
  var island_list = [];
  Rand.seed = Rand.seed * 16807 % 2147483647;
  island_list.push({'x': 0, 'y': 0, 'seed': (Rand.seed & 1073741823) % 100000, 'range': 0});
  var islandMax = plMax * 2 + 1;
  var meanDistance = 200;   
  var deviationDistanceMax = 35;
  var nb_try = 0;                            
  var actual_range = 2;
  while (island_list.length < islandMax) {
    Rand.seed = Rand.seed * 16807 % 2147483647;
    var pickedIsland = island_list[(Rand.seed & 1073741823) % island_list.length];
    Rand.seed = Rand.seed * 16807 % 2147483647;
    var rndAngle = (((Rand.seed & 1073741823) % 10007) / 10007.0) * 6.28;
    var pos_x = pickedIsland.x + Math.cos(rndAngle) * meanDistance;
    var pos_y = pickedIsland.y + Math.sin(rndAngle) * meanDistance;
    var islandValide = true;
    if (pickedIsland.range < actual_range) {
      var i = 0;
      while (i < island_list.length) {
        var island = island_list[i];
        ++i;
        var diff_x = island.x - pos_x;
        var diff_y = island.y - pos_y;
				var distance = Math.sqrt(diff_x * diff_x + diff_y * diff_y);
				var deviationDistance = Math.abs(distance - meanDistance);
				// Si l'ile est proche ça distance doit être comprise
				// entre 235 et 165
				if (deviationDistance > deviationDistanceMax && distance < 300) {
					islandValide = false;
					break;
				}
			}
			if (islandValide) {
				Rand.seed = Rand.seed * 16807 % 2147483647;
				island_list.push({'x': Std.int(pos_x), 'y': Std.int(pos_y), 'seed': (Rand.seed & 1073741823) % 100000, 'range': pickedIsland.range + 1});
				nb_try = 0;
			}
		}
		++nb_try;
		if (nb_try > 180) {
			++actual_range;
		}
	}
	marginLength = 80;
	xmin = 0;
	xmax = 0;
	ymin = 0;
	ymax = 0;
	i = 0;
	while (i < island_list.length) {
		       island = island_list[i];
       ++i;
       xmin = Math.min(island.x, xmin);
       xmax = Math.max(island.x, xmax);
       ymin = Math.min(island.y, ymin);
       ymax = Math.max(island.y, ymax);
     }
     width = Std.int(xmax + 2 * marginLength - xmin);
     height = Std.int(ymax + 2 * marginLength - ymin);
     v19 = 0;
     v20 = 0;
     if (width < ;Q[2{.=G=5w) {
       v19 = ;Q[2{.=G=5w - width;
     }
     if (height < ;Q[2{.6_b=2() {
       v20 = ;Q[2{.6_b=2( - ;Q[2{.4Zb4+( - height;
     }
     i = 0;
		 //Decale toutes les iles
     while (i < island_list.length) {
       island = island_list[i];
       ++i;
       island.x += Std.int(v19 * 0.5 + marginLength - xmin);
       island.y += Std.int(v20 * 0.5 + marginLength - ymin);
     }
     width += Std.int(v19);
     height += Std.int(v20);
     return {'width': width, 'height': height, 'list': island_list};
   }
   catch (nb_try) {
     v4 = nb_try;
     haxe.6z7-o.trace(Std.string(v4), {'fileName': 'Tools.hx', 'lineNumber': 74, 'className': 'Tools', 'methodName': 'buildMap2'});
     haxe.6z7-o.trace((haxe.Stack.FGbzN()).join('\n'), {'fileName': 'Tools.hx', 'lineNumber': 75, 'className': 'Tools', 'methodName': 'buildMap2'});
     throw v4;
   }
 };

 v1.__class__.1j98= = function (arg0) {


