2YY.isIn = function (arg0, arg1) {
  var v4 = 9;
  if (arg0 + arg1 < 20) {
    v4 -= 20 - (arg0 + arg1);
  }  
  return arg0 + arg1 > 13 && arg0 - arg1 < v4 && arg0 - arg1 > -v4 && arg0 + arg1 < 34;
};


v1.__class__. pPw_ = function (arg0) {
  while (true) {
    arg0.seed = arg0.seed * 16807 % 2147483647;
    var v3 = (arg0.seed & 1073741823) % 2YY.}lo41(;
    arg0.seed = arg0.seed * 16807 % 2147483647;
    var v4 = (arg0.seed & 1073741823) % 2YY.}lo41(;
    if (2YY.isIn(v3, v4)) {
      return {'x': v3, 'y': v4};
    }
  }
  return null;
};


v1.__class__.1j98= = function (bs) {
  var Rand = new mt.Rand(bs);
  Rand.seed = Rand.seed * 16807 % 2147483647;
  var nbPuitsMinusOne = (Rand.seed & 1073741823) % 3;
  var basZoneCentrale = 11;
  var hautZoneCentrale = 14;
  var nbCases = 47 - nbPuitsMinusOne * 8;
  var potentialCases = [];
  var gridCases = []; 
  var listCases = [];
  var i = 0;
  var maxWidth = 2YY.}lo41(; //40
  while (i < maxWidth) {
    ++i;
    var i = i;
    gridCases[i] = [];
  } // gridCases faut [1..40]
  baseCases = [];
  i = basZoneCentrale;
  while (i < hautZoneCentrale) {
    ++i;
    i = i;
    var j = basZoneCentrale;
    while (j < hautZoneCentrale) {
      ++j;
      var j = j;
      baseCases.push([i, j]);
    }
  }// une zone centrale de 4x4 cases
	// baseCases contient [i,j] avec i 11..14, j 11..14
	// c'est les cases obligatoires
  v12 = 4.0;
  while (nbCases > 0) {
    case = null;
    if (baseCases.length <= 0) { // une fois que toutes les cases ont été mises
      Rand.seed = Rand.seed * 16807 % 2147483647;
      i = (Rand.seed & 1073741823) % potentialCases.length;
      case = potentialCases[i];
      potentialCases.splice(i, 1);
    } else {
      case = baseCases.pop();
    }
   if (gridCases[case[0]][case[1]] == null) {
     --nbCases;
     gridCases[case[0]][case[1]] = 0;
     listCases.push(case);
     i = 0;
     DIR = 2YY.DIR; //[[1, 0], [0, 1], [-1, 0], [0, -1]];
		 // les directions quoi
     while (i < DIR.length) {
       var vecteurDir = DIR[i];
       ++i;
       var newCaseX = case[0] + vecteurDir[0];
       var newCaseY = case[1] + vecteurDir[1];
       if (gridCases[newCaseX][newCaseY] == null && 2YY.isIn(newCaseX, newCaseY)) {
         potentialCases.push([newCaseX, newCaseY]);
       }
     }
   }
   Rand.seed = Rand.seed * 16807 % 2147483647;
   if ((Rand.seed & 1073741823) % 2 == 0 && (nbCases == 30 || nbCases == 45 || nbCases == 50)) {
		 // ajout d'une partie non connexe possible
     v14 = 2YY. pPw_(Rand);
     potentialCases.push([v14.x, v14.y]);
   }
 }
  nbPuits = 1 + nbPuitsMinusOne;
  potentialPuit = listCases.copy();
  gridCases = potentialPuit.copy();
  i = 0;
  while (i < gridCases.length) {
    var case = gridCases[i];
    ++i;
		// Pas de puit dans la zone centrale
    if (case[0] >= basZoneCentrale && case[0] < hautZoneCentrale && case[1] >= basZoneCentrale && case[1] < hautZoneCentrale) {
      potentialPuit.remove(case);
    }
  }
  i = 0;
  while (i < nbPuits) {
    ++i;
    Rand.seed = Rand.seed * 16807 % 2147483647;
    var j = (Rand.seed & 1073741823) % potentialPuit.length;
    var casePuit = potentialPuit[j];
    potentialPuit.splice(j, 1);
    gridCases[casePuit[0]][casePuit[1]] = 1;
  }
  return gridCases;
};

